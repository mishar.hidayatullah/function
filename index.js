// Import readline
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const cuboid = require('./day 7/tube'); // import tube
const cube = require('./day 7/cube'); // import cube
const kerucut = require('./day 7/kerucut'); // import cube

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function to display the menu
function menu() {
  console.log(`Menu`);
  console.log(`====`);
  console.log(`1. Tube`);
  console.log(`2. Cube`);
  console.log(`3. Kerucut`);
  console.log(`4. Exit`);
  rl.question(`Choose option: `, (option) => {
    if (!isNaN(option)) {
      // If option is a number it will go here
      if (option == 1) {
        tube.inputLength(); // It will call input() function in tube file
      } else if (option == 2) {
        cube.input(); // It will call input() function in cube file
      } else if (option == 3) {
        kerucut.input(); // It will call input() function in kerucut file
      } else if (option == 4) {
        rl.close(); // It will close the program
      } else {
        console.log(`Option must be 1 to 4!\n`);
        menu(); // If option is not 1 to 4, it will go back to the menu again
      }
    } else {
      // If option is not a number it will go here
      console.log(`Option must be number!\n`);
      menu(); // If option is not 1 to 4, it will go back to the menu again
    }
  });
}

menu(); // call the menu function to display the menu

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
