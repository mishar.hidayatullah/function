const index = require("../index");

function tube(r, t) {
  return 2 * 3.14 * r * (r + t);
}

function inputTube() {
  index.rl.question("jari-jari: ", (r) => {
    index.rl.question("Tinggi: ", (t) => {
      if (!isNaN(r) && !isNaN(t)) {
        console.log(`\nTube: ${tube(r, t)} \n`);
        index.rl.close();
      } else {
        console.log(`r and t must be a number\n`);
        inputTube();
      }
    });
  });
}

module.exports = { inputTube };
